// Andre de Sousa Costa Filho - 15/0005521
// Filipe Coelho Hilario - 15/0059213

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#define MAX 30
#define PLACEHOLDER '$' /* 100% */

struct x {
    char digito;
    struct x *prox;
};
typedef struct x Lista;

Lista* aloca() {
    return (Lista*) malloc(sizeof(Lista));
}

/* 100% */
Lista* add(Lista* head, char arg) {
    Lista* novo = aloca();
    novo->digito = arg;
    novo->prox = head;
    head = novo;
    return head;
}

Lista* annihilate(Lista* head) {
    Lista *p = head;
    head = head->prox;
    free(p);
    return head;
}

void listaKiller(Lista *head) {
    Lista *p = head;
    while(p->prox == NULL) {
        
    }
}

char* infixPosfix(char infixa[], Lista* pilha) {
    char *posfixa;
    int tamanho, i = 0, j;

    tamanho = strlen(infixa);
    posfixa = (char*) malloc(tamanho * sizeof(char));

    // pilha[i++] = infixa[0];
    pilha = add(pilha, infixa[0]);

    for (j = 0, i = 1; infixa[i] != '\0'; ++i) {
        switch (infixa[i]) {
            char x;
            /* empilha  a bagaça*/
            case '(':
            // pilha[topo++] = infixa[i];
            pilha = add(pilha, infixa[i]);
            break;
            /* desempilha a bagaça */
            case ')':
            while (1) {
                //vai salvando ate encontrar o ) e então sai do laço
                x = pilha->digito;
                pilha = annihilate(pilha);
                if (x == '(') {
                    break;
                }
                posfixa[j++] = x;
            }
            break;

            /* desempilha e depois empilha */
            case '+':
            case '-':
            while (1) {
                // x = pilha[topo - 1];
                x = pilha->digito;
                pilha = annihilate(pilha);
                if (x == '(') {
                    break;
                }
                // pilha[topo++] = infixa[i];
                pilha = add(pilha, infixa[i]);
            }
            break;

            /**/
            case '*':
            case '/':
            while (1) {
                // x = pilha[topo-1];
                x = pilha->digito;
                pilha = annihilate(pilha);
                if (x == '(' || x == '+' || x =='-') {
                    break;
                }
                posfixa[j++] = x;
            }
            // pilha[topo++] = infixa[i];
            pilha = add(pilha, infixa[i]);
            break;

            default:
            posfixa[j++] = infixa[i];
            break;
        }
    }
    free(pilha);
    posfixa[j] = '\0';
    return posfixa;
}

int main() {
    char expressao[MAX];
    Lista* lista = aloca();
    lista->digito = PLACEHOLDER;
    lista->prox = NULL;


    return 0;
}
